# Proyecto Desafio Fravega

Desafío propuesto por el cliente Fravega.

## Requisitos necesario para ejecutar la aplicación
* [Java jdk 11](https://www.oracle.com/ar/java/technologies/javase/jdk11-archive-downloads.html) - Debe tener configurada la variable de entorno [JAVA_HOME](https://confluence.atlassian.com/doc/setting-the-java_home-variable-in-windows-8895.html)
* [Docker](https://www.docker.com/get-started) 
* (opcional) Maven 3.8.3 - El proyecto proporciona Maven wrapper.
* [GIT 2.33](https://git-scm.com/downloads)
  
## **1- Descargar el proyecto**
---
```
git clone git@gitlab.com:igutierrez-projects/desafio-fravega.git
```
## **2- Compilar el proyecto**
---
Abrir console de comandos y ubicarse sobre la carpeta en la que se encuentra el proyecto descargado.
Generar el archov .jar del proyecto mediante Maven ejecutando el siguiente comando.
```
mvn clean compile install
```
Es necesario tener previamente instalado Maven, en caso de no tenerlo puede generar el .jar mediante la ejecución del sigiuente commando (Sistema Operativo Windows 10)
```
./mvnw.cmd clean compile install
```
Se generara el archivo .jar en la carpeta **/target**

## **3-  Deploy del proyecto**
---
Se presenta 2 procecimientos para poder ejecutar la aplicacion:
* Mediante docker-compose
* Comandos docker   

Los mismos son descriptos a continuación:
### Docker-compose
Abrir una consola y ubicarse en la carpeta /desafio-fravega, luego ejecute el siguiente comando.
```
docker-compose -f docker-compose.yml up
```

### Comandos docker
---

```
docker pull mysql
```
```
docker run -d -p 13306:3306 --name mysql_fravega_container -e MYSQL_ROOT_PASSWORD=secret -e MYSQL_DATABASE=fravega_db -e MYSQL_USER=fravega_user -e MYSQL_PASSWORD=secret -d mysql:5.7
```
### Levantar el proyecto desde Dockerfile
---
Una vez que se tiene el servidor de base de datos ejecutandose, se debe proceder levantar la aplicación. Esto se va realizar mediante docker. Siguiendo los siguientes pasos:
1- Abrir la consolta y ubicarse sobre la carpeta en la que se encuentra el proyecto descargado.
2- Se debe generar el .jar del proyecto mediante Maven ejecutando el siguiente comando.
```
mvn clean compile install
```
Es necesario tener previamente instalado Maven, en caso de no tenerlo puede generar el .jar mediante la ejecución del sigiuente commando (Sistema Operativo Windows 10)
```
mvnw.cmd clean compile install
```
3- Una vez completado el comando anterior, debe proceder a contruir la imagen docker correspondiente al proyecto ejecutando el siguiente comando
```
docker build . -t fravega-app-image
```
4- Finalizado la construccion del proyecto, se puede levantar la aplicación mediante el siguiente comando
```
docker run -it  -p 8001:8080 --name fravega-app-container -e SPRING_PROFILES_ACTIVE=production -e FRAVEGA_DATASOURCE_URL="jdbc:mysql://desafio-fravega_mysql_fravega_container_1:3306/fravega_db?allowPublicKeyRetrieval=true&useSSL=false" -e FRAVEGA_DATASOURCE_USER=fravega_user -e FRAVEGA_DATASOURCE_PASSWORD=secret --link mysql_fravega_container:mysql  fravega-app-image
```

**El servicio quedar expuesto en http://localhost:8001/api/nodo**



## Descripción del servicio y datos de prueba
El proyecto presenta una aplicación restfull que contas de los siguientes endpoing
* POST /api/nodo/ : Permite registrar una nueva sucursal o Punto de retiro.

[Sucursal 2](https://www.google.com.ar/maps/place/31%C2%B024'38.4%22S+64%C2%B010'46.3%22W/@-31.6466456,-60.7098175,15z/data=!4m5!3m4!1s0x0:0xa079fd1cf01f5e3f!8m2!3d-31.4106736!4d-64.1795139)
```
  {        
        "latitude": -31.4106736,
        "longitude": -64.1795139,
        "nodeType": "STORE",
        "openTime": "08:00",
        "closeTime": "18:00",
        "address": "Domingo F Sarmiento 141"
    }
```
[Sucursal 1](https://www.google.com.ar/maps/place/31%C2%B026'38.1%22S+64%C2%B011'44.7%22W/@-31.4439017,-64.1963074,19z/data=!3m1!4b1!4m5!3m4!1s0x0:0xcd25a453f123021f!8m2!3d-31.4439028!4d-64.1957602)
```
  {
        
        "latitude": -31.443902761980677,
        "longitude": -64.19576017273398,
        "nodeType": "STORE",
        "openTime": "08:00",
        "closeTime": "18:00",
        "address": "Lizando Novillo Saravia - Espacio Quality"
    }
```

[Punto de retiro 1](https://www.google.com.ar/maps/place/31%C2%B027'32.5%22S+64%C2%B012'53.5%22W/@-31.459022,-64.2154129,19z/data=!3m1!4b1!4m5!3m4!1s0x0:0x9328d26e9997f1a2!8m2!3d-31.4590231!4d-64.2148657)
```
{
        "latitude": -31.459023071612258,
        "longitude": -64.21486571569412,
        "nodeType": "STORE_PICKUP",
        "capacity": 10000
    }
```
[Punto de retiro 1](https://www.google.com.ar/maps/place/31%C2%B028'25.2%22S+64%C2%B013'29.9%22W/@-31.4736644,-64.2255239,19z/data=!3m1!4b1!4m5!3m4!1s0x0:0xec978925d78074a5!8m2!3d-31.4736655!4d-64.2249767)
```
{
        "latitude": -31.473665486632783,
        "longitude": -64.22497666021866,
        "nodeType": "STORE_PICKUP",
        "capacity": 10000
    }
```
* **PUT /api/nodo/** : permite modificar una sucursal o punto de retiro.
* **DELETE /api/nodo** : permite eliminar una sucursal o punto de retiro.
* **GET /api/nodo/{id}** : permite obtener información de una sucursal o punto de retiro.
* **GET /api/nodo/listar** : permite listar las sucursales y puntos de retiros registrados.
* **GET /api/nodo/proximo/{latitud}/{longitud}** : permite obtener la sucursal o punto de retiro mas proximo a una latitud y longitud dada.

[Ejemplo de prueba](https://www.google.com.ar/maps/place/31%C2%B024'34.6%22S+64%C2%B011'00.5%22W/@-31.4096176,-64.1840047,19z/data=!3m1!4b1!4m5!3m4!1s0x0:0x953f7220db0aa928!8m2!3d-31.4096187!4d-64.1834575)

```
Latitud : -31.40961867675207
Longitud : -64.18345748051959
```
La punto mas cerca es la Sucursal **Domingo F Sarmiento 141**

---
Se encuentra información disponible proporcionada por swagger. El enpoint es 
**http://localhost:8001/swagger-ui/index.html**

---



Se manejan tiene disponibles 3 niveles de usuario:
* **testing** : Utilizado para correr los testing unitarios usando H2 database.
* **develop**  : Utilizado para realizar desarrollos, con nivel de logueo DEBUG. Opera sobre la base de datos MYSQL
* **production** : Perfil utilizado por defecto en el archivo ***docker-compose.yml***. Opera sobre la base de datos MYSQL
