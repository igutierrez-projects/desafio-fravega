package com.springboot.fravega.app.store.controller;

import org.springframework.http.HttpStatus;

import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@RequiredArgsConstructor
@Getter
@Setter
public class ErrorResponse {
	
	private int status;
	private String error;
	private String message;
	
	@Builder
	public ErrorResponse(HttpStatus httpStatus, String message) {
		this.status= httpStatus.value();
		this.error = httpStatus.name();
		this.message = message;
	}
	

}
