package com.springboot.fravega.app.store.routes;

import org.springframework.stereotype.Component;

@Component("haversineFormula")
public class HaversineFormula implements ApiRoutes {
	
	private static final double EARTH_RADIUS = 6371;

	@Override
	public Double getDistance(GeographicCoordinate originPoint, GeographicCoordinate endPoint) {
		// TODO Auto-generated method stub
		double latitudeOriginRadians = Math.toRadians(originPoint.getLatitude());
		double latitudeEndRadians = Math.toRadians(endPoint.getLatitude());
		double deltaLatitude = latitudeOriginRadians - latitudeEndRadians;  
		double deltaLongitude = Math.toRadians(originPoint.getLongitude()) - Math.toRadians(endPoint.getLongitude());
		
		double squareTerm = Math.sqrt(Math.pow(Math.sin(deltaLatitude/2),2)+Math.cos(latitudeOriginRadians)*Math.cos(latitudeEndRadians)*Math.pow(Math.sin(deltaLongitude/2),2));
		double asinTerm = 2 * Math.asin(squareTerm) ;
		double distance = asinTerm * EARTH_RADIUS;
		return distance;
	}
	
	

}
