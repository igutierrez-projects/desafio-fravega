package com.springboot.fravega.app.store.exceptions;

public class DataFormatException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public DataFormatException(String message, Throwable throwable) {
		super(message, throwable);
	}
	
	public DataFormatException(String message) {
		super(message);
	}


}
