package com.springboot.fravega.app.store.exceptions;

public class EntityAlreadyExistException  extends RuntimeException{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public EntityAlreadyExistException(String message, Throwable throwable) {
		super(message, throwable);
	}
	
	public EntityAlreadyExistException(String message) {
		super(message);
	}

}
