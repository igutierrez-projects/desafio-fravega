package com.springboot.fravega.app.store.dto;


import com.fasterxml.jackson.annotation.JsonTypeName;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


//@ApiModel(parent = NodeDTO.class, description = "Clase que representa un Nodo Sucursal")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@JsonTypeName("STORE")
public class StoreDTO extends NodeDTO{
	private String openTime;
	private String closeTime;
	private String address;

}

