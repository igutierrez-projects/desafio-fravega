package com.springboot.fravega.app.store.entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import lombok.Builder;
import lombok.EqualsAndHashCode;

@Entity
@DiscriminatorValue("1")
@EqualsAndHashCode(callSuper = true)
public class StorePickup extends Node{

	private Long capacity;	

	public StorePickup() {
		super();
	}

	@Builder
	public StorePickup(Double latitude, Double longitude, Long capacity) {
		super(latitude, longitude, NodeType.STORE_PICKUP);
		this.capacity = capacity;
	}

	public Long getCapacity() {
		return capacity;
	}

	public void setCapacity(Long capacity) {
		this.capacity = capacity;
	}	
	
}
