package com.springboot.fravega.app.store.model.service;


import java.util.List;

import com.springboot.fravega.app.store.entity.Node;
public interface INodeService {
	
	Node saveNode(Node node);
	
	Node getNode(Integer id);
	
	void deleteNode(Integer id);
	
	Node updateNode(Node sucursal);
	
	Node getNearestNode(Double latitude, Double longitude);
	
	List<Node> listarNodos();
	

}
