package com.springboot.fravega.app.store.utils;

import java.time.DateTimeException;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

import com.springboot.fravega.app.store.exceptions.DataFormatException;

public class UtilFravega {	

	public static String parseLocalTimeToString(LocalTime time, String format) {		
		try {
		return time.format(DateTimeFormatter.ofPattern(format));
		} catch(DateTimeException ex) {
			throw new DataFormatException(String.format("No se puede formatear objeto a formato %s",format));
		}
				
	}
	
	public static LocalTime parseStringToLocalTime(String time, String format)  {		
		try {
		LocalTime localTime = LocalTime.parse(time, DateTimeFormatter.ofPattern(format));
		return localTime;	
		} catch(DateTimeParseException e) {
			throw new DataFormatException(String.format("Formato de tiempo incorrecto. Debe ser %s",format));
		}
			
	}
}
