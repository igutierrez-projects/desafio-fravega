package com.springboot.fravega.app.store.model.service;

import java.util.List;

import com.springboot.fravega.app.store.dto.NodeDTO;
import com.springboot.fravega.app.store.entity.Node;

public interface INodeDTOModelMapperService {
	
	NodeDTO mapHeritageNode(Node node);
	
	Node mapHeritageNodeDTO(NodeDTO nodeDTO);
	
	List<Node> mapHeritageNodeDTO(List<NodeDTO> nodeDTO);
	
	List<NodeDTO> mapHeritageNode(List<Node> node);

}
