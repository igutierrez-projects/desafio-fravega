package com.springboot.fravega.app.store.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Entity
@Table(name="Nodo")
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="node_type",
					discriminatorType = DiscriminatorType.INTEGER
					)
@AllArgsConstructor
@NoArgsConstructor
@RequiredArgsConstructor
@EqualsAndHashCode
@Getter
@Setter
public abstract class Node {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@NonNull
	private Double latitude;
	@NonNull
	private Double longitude;
	@Column(name="create_at", columnDefinition = "TIMESTAMP")
	@Temporal(TemporalType.TIMESTAMP)	
	private Date createAt;	
	@Enumerated(EnumType.ORDINAL)
	@Column(name="node_type", insertable = false, updatable = false)
	@NonNull
	private NodeType nodeType;
		
		
	
	
	@PrePersist
	private void onCreate() {
		this.setCreateAt(new Date((new Date()).getTime()));
	}
}
