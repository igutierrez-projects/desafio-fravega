package com.springboot.fravega.app.store;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
public class DesafioFravegaApplication {

	public static void main(String[] args) {
		SpringApplication.run(DesafioFravegaApplication.class, args);
	}

}
