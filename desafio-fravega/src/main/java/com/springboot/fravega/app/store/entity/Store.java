package com.springboot.fravega.app.store.entity;

import java.time.LocalTime;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@DiscriminatorValue("0")
@EqualsAndHashCode(callSuper = true)
public class Store extends Node{

	private LocalTime openTime;
	private LocalTime closeTime;
	private String address;
	
	@Builder
	public Store(Double latitude, Double longitude, LocalTime openTime, LocalTime closeTime, String address) {
		super(latitude, longitude, NodeType.STORE);
		this.openTime = openTime;
		this.closeTime = closeTime;
		this.address = address;
	}
	
	public Store() {
		super();
	}	
}
