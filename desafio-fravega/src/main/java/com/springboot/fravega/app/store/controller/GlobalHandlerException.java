package com.springboot.fravega.app.store.controller;


import org.modelmapper.MappingException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.springboot.fravega.app.store.exceptions.DataFormatException;
import com.springboot.fravega.app.store.exceptions.EntityAlreadyExistException;
import com.springboot.fravega.app.store.exceptions.EntityNotExistException;

import lombok.extern.log4j.Log4j2;

@Log4j2
@ControllerAdvice
public class GlobalHandlerException extends ResponseEntityExceptionHandler {

	
	@ExceptionHandler(EntityNotExistException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public ResponseEntity<ErrorResponse> handleEntityNotExistException(EntityNotExistException exception, WebRequest request){
		log.error(exception.getMessage(), exception);
		return this.buildErrorResponse(exception, HttpStatus.NOT_FOUND, request);
		
	}
	
	@ExceptionHandler(Exception.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	public ResponseEntity<ErrorResponse> handleAllUncaughtException(Exception exception, WebRequest request){
		log.error(exception.getMessage(), exception);
		return this.buildErrorResponse(exception, HttpStatus.INTERNAL_SERVER_ERROR, request);
		
	}
	
	@ExceptionHandler({IllegalArgumentException.class,
						MappingException.class,
						DataFormatException.class})
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ResponseEntity<ErrorResponse> handleIllegalArgumentException(Exception exception, WebRequest request){
		log.error(exception.getMessage(), exception);
		return this.buildErrorResponse(exception, HttpStatus.BAD_REQUEST, request);
	
	}
	
	@ExceptionHandler(EntityAlreadyExistException.class)
	@ResponseStatus(HttpStatus.CONFLICT)
	public ResponseEntity<ErrorResponse> handleEntityAlreadyExistException(Exception exception, WebRequest request){
		log.error(exception.getMessage(), exception);
		return this.buildErrorResponse(exception, HttpStatus.CONFLICT, request);
		
	}
	
	
	private ResponseEntity<ErrorResponse> buildErrorResponse(Exception exception, HttpStatus httpStatus, WebRequest request){
		ErrorResponse errorResponse = ErrorResponse.builder()
													.httpStatus(httpStatus)
													.message(exception.getMessage())
													.build();
		return ResponseEntity.status(httpStatus).body(errorResponse);
		
	}
	
}
