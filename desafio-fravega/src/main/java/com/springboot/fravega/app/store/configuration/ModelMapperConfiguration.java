package com.springboot.fravega.app.store.configuration;

import java.time.LocalTime;

import org.modelmapper.AbstractConverter;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.springboot.fravega.app.store.dto.StoreDTO;
import com.springboot.fravega.app.store.entity.Store;
import com.springboot.fravega.app.store.utils.UtilFravega;

@Configuration
public class ModelMapperConfiguration {
	
	@Bean
	public ModelMapper modelMapper() {
		ModelMapper modelMapper = new ModelMapper();		
		modelMapper.addMappings(propetyMapStoreToStoreDto());
		modelMapper.addMappings(propetyMapStoreDTOToStore());
		
		return modelMapper;
	}
	

	private PropertyMap<Store,StoreDTO> propetyMapStoreToStoreDto(){
		Converter<LocalTime, String> timeToStringConverter = new AbstractConverter<LocalTime, String>() {
			@Override
			protected String convert(LocalTime source) {				
				return UtilFravega.parseLocalTimeToString(source, "HH:mm");				
			}			
		};
				
		PropertyMap<Store,StoreDTO> propertyMap = new PropertyMap<Store, StoreDTO>() {

			@Override
			protected void configure() {
				using(timeToStringConverter).map(source.getOpenTime(), destination.getOpenTime());
				using(timeToStringConverter).map(source.getCloseTime(), destination.getCloseTime());
			}
			
		};
		
		return propertyMap;		
	}
	
	private PropertyMap<StoreDTO,Store> propetyMapStoreDTOToStore(){
		Converter<String, LocalTime> stringToTimeConverter = new AbstractConverter<String, LocalTime>() {
			@Override
			protected LocalTime convert(String source) {				
				return UtilFravega.parseStringToLocalTime(source, "HH:mm");				
			}			
		};
		
		
		PropertyMap<StoreDTO,Store> propertyMap = new PropertyMap<StoreDTO, Store>() {

			@Override
			protected void configure() {
				using(stringToTimeConverter).map(source.getOpenTime(), destination.getOpenTime());
				using(stringToTimeConverter).map(source.getCloseTime(), destination.getCloseTime());
			}
			
		};
		
		return propertyMap;		
	}
	

}
