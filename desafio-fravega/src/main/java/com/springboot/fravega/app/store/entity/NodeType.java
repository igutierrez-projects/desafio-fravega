package com.springboot.fravega.app.store.entity;

import java.util.Arrays;

public enum NodeType {
	STORE(0,"Sucursal"),
	STORE_PICKUP(1, "Punto de retiro");
	
	private final Integer id;
	private final String description;
	private NodeType(Integer id, String description) {
		this.id = id;
		this.description = description;
	}
	
	
	public Integer getId() {
		return this.id;
	}
	
	public String getDescription() {
		return this.description;
	}
	
	public static NodeType parse(Integer id) {
		return Arrays.stream(NodeType.values()).filter(x -> x.getId().equals(id)).findFirst().orElse(null);
				}

}
