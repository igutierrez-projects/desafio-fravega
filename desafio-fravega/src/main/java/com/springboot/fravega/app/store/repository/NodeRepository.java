package com.springboot.fravega.app.store.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.springboot.fravega.app.store.entity.Node;

public interface NodeRepository extends CrudRepository<Node, Integer>{

	List<Node> findByLatitudeAndLongitude(Double latitude, Double longitude);
}
