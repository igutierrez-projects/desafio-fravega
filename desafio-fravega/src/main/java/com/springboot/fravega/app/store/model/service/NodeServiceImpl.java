package com.springboot.fravega.app.store.model.service;

import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.springboot.fravega.app.store.entity.Node;
import com.springboot.fravega.app.store.exceptions.EntityAlreadyExistException;
import com.springboot.fravega.app.store.exceptions.EntityNotExistException;
import com.springboot.fravega.app.store.repository.NodeRepository;
import com.springboot.fravega.app.store.routes.ApiRoutes;
import com.springboot.fravega.app.store.routes.GeographicCoordinate;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Service("nodeServiceImpl")
public class NodeServiceImpl implements INodeService {
	
	@Autowired
	private NodeRepository nodeRepository;
	
	@Autowired
	@Qualifier("haversineFormula")
	private ApiRoutes apiRoutes;

	@Override
	@Transactional
	public Node saveNode(Node node) {		
		List<Node> nodeList = nodeRepository.findByLatitudeAndLongitude(node.getLatitude(), node.getLongitude());
		if(!nodeList.isEmpty()) {
			throw new EntityAlreadyExistException("La coordenadas ingresadas ya existen en el sistema");
		}
		return nodeRepository.save(node); 
	}
	

	@Override
	@Transactional(readOnly = true)
	public Node getNode(Integer id) {
		Node node = nodeRepository.findById(id).orElseThrow(()->new EntityNotExistException("Nodo no existe."));
		
		return node;
	}

	@Override
	public void deleteNode(Integer id) {
		nodeRepository.deleteById(id);
		
	}

	@Override
	public Node updateNode(Node node) {
		Objects.requireNonNull(node.getId(), "Nodo no identificado");
		nodeRepository.findById(node.getId()).orElseThrow(() -> new EntityNotExistException("El nodo no se encentra registrado."));
		return nodeRepository.save(node);
	}

	@Override
	public Node getNearestNode(Double latitude, Double longitude) {
		GeographicCoordinate coordinate = GeographicCoordinate.builder()
														.latitude(latitude)
														.longitude(longitude)
														.build();
		
		Iterable<Node> nodesIterable = nodeRepository.findAll(); 
		Stream<Node> nodeStream = StreamSupport.stream(nodesIterable.spliterator(), false);
		List<NodeDistanceObject> distanceList = nodeStream.map( node -> {	
			GeographicCoordinate coordinateNode = new GeographicCoordinate(node.getLatitude(), node.getLongitude());
			return new NodeDistanceObject(node, apiRoutes.getDistance(coordinate, coordinateNode));}).collect(Collectors.toList());
		
		Optional<NodeDistanceObject> nearestNode = distanceList.stream().min(Comparator.comparing(NodeDistanceObject::getDistance));
		return nearestNode.get().getNode();
	}

	@Override
	public List<Node> listarNodos() {
		return StreamSupport.stream(nodeRepository.findAll().spliterator(), false).collect(Collectors.toList());
	}

}

@Getter
@Setter
@AllArgsConstructor
class NodeDistanceObject{
	private Node node;
	private Double distance;
}