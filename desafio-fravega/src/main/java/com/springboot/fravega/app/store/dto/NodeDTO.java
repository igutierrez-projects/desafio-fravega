package com.springboot.fravega.app.store.dto;



import org.modelmapper.ModelMapper;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.springboot.fravega.app.store.entity.Node;
import com.springboot.fravega.app.store.entity.NodeType;
import com.springboot.fravega.app.store.entity.Store;
import com.springboot.fravega.app.store.entity.StorePickup;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
//@ApiModel(subTypes = {StoreDTO.class, StorePickupDTO.class}, discriminator = "nodeType", description = "Clase padre de tipos de nodos Sucursales, Punto Retiro.")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.EXISTING_PROPERTY, property = "nodeType", visible = true)
@JsonSubTypes({
    @JsonSubTypes.Type(value = StoreDTO.class, name = "STORE"),
    @JsonSubTypes.Type(value = StorePickupDTO.class, name = "STORE_PICKUP")
})
public abstract class NodeDTO {
	
	private Integer id;
	private Double latitude;
	private Double longitude;
	private NodeType nodeType;
	
	public static NodeDTO mapHeritageNode(Node node, ModelMapper mapper){		
		
		if(node instanceof Store) {
			return mapper.map(node, StoreDTO.class);
		} else if (node instanceof StorePickup) {
			return mapper.map(node, StorePickupDTO.class);
		} else {
			return mapper.map(node, NodeDTO.class);
		}
	}
	
	public static Node mapHeritageNodeDTO(NodeDTO nodeDTO, ModelMapper mapper){		
		
		if(nodeDTO instanceof StoreDTO) {
			return mapper.map(nodeDTO, Store.class);
		} else if (nodeDTO instanceof StorePickupDTO) {
			return mapper.map(nodeDTO, StorePickup.class);
		} else {
			return mapper.map(nodeDTO, Node.class);
		}
	}

}
