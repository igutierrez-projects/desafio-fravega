package com.springboot.fravega.app.store.routes;

public interface ApiRoutes {

	Double getDistance(GeographicCoordinate originPoint, GeographicCoordinate endPoint);
}
