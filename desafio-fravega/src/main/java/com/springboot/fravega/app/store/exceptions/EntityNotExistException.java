package com.springboot.fravega.app.store.exceptions;

public class EntityNotExistException extends RuntimeException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public EntityNotExistException(String message, Throwable throwable) {
		super(message, throwable);
	}
	
	public EntityNotExistException(String message) {
		super(message);
	}

}
