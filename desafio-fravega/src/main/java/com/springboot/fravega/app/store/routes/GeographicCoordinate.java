package com.springboot.fravega.app.store.routes;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class GeographicCoordinate {
	
	private Double latitude;
	private Double longitude;
	
}
