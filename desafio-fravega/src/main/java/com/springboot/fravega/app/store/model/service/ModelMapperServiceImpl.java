package com.springboot.fravega.app.store.model.service;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springboot.fravega.app.store.dto.NodeDTO;
import com.springboot.fravega.app.store.dto.StoreDTO;
import com.springboot.fravega.app.store.dto.StorePickupDTO;
import com.springboot.fravega.app.store.entity.Node;
import com.springboot.fravega.app.store.entity.Store;
import com.springboot.fravega.app.store.entity.StorePickup;

@Service("modelMapperServiceImpl")
public class ModelMapperServiceImpl implements INodeDTOModelMapperService{
	
	@Autowired
	private ModelMapper modelMapper;	

	@Override
	public NodeDTO mapHeritageNode(Node node) {
		if(node instanceof Store) {
			return modelMapper.map(node, StoreDTO.class);
		} else if (node instanceof StorePickup) {
			return modelMapper.map(node, StorePickupDTO.class);
		} else {
			return modelMapper.map(node, NodeDTO.class);
		}
	}

	@Override
	public Node mapHeritageNodeDTO(NodeDTO nodeDTO) {
		if(nodeDTO instanceof StoreDTO) {
			return modelMapper.map(nodeDTO, Store.class);
		} else if (nodeDTO instanceof StorePickupDTO) {
			return modelMapper.map(nodeDTO, StorePickup.class);
		} else {
			return modelMapper.map(nodeDTO, Node.class);
		}
	}

	@Override
	public List<Node> mapHeritageNodeDTO(List<NodeDTO> nodeDTO) {		
		List<Node> list = nodeDTO.stream().map(model -> this.mapHeritageNodeDTO(model)).collect(Collectors.toList());
		return list;
	}

	@Override
	public List<NodeDTO> mapHeritageNode(List<Node> node) {
		List<NodeDTO> list = node.stream().map(object -> this.mapHeritageNode(object)).collect(Collectors.toList());
		return list;
	}

}
