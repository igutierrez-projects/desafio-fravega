package com.springboot.fravega.app.store.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.springboot.fravega.app.store.dto.NodeDTO;
import com.springboot.fravega.app.store.entity.Node;
import com.springboot.fravega.app.store.model.service.INodeDTOModelMapperService;
import com.springboot.fravega.app.store.model.service.INodeService;

import io.swagger.annotations.ApiOperation;



@RestController
@RequestMapping("/api/nodo")
public class NodeController {
	
	@Autowired
	@Qualifier("nodeServiceImpl")
	private INodeService nodeService;
	
	@Autowired
	@Qualifier("modelMapperServiceImpl")
	private INodeDTOModelMapperService modelMapper;	
	
	
	@ApiOperation (value = "Consulta de información", notes =  "Consulta de información de una Sucursal o Punto de retiro especifico.")
	@GetMapping("{id}")
	public NodeDTO getNode(@PathVariable Integer id) {
		try {
		Node node = nodeService.getNode(id);			
		return modelMapper.mapHeritageNode(node);
		} catch(NullPointerException ex) {
			throw new ResponseStatusException(
			           HttpStatus.NOT_FOUND, "Node Not Found", ex);
		}
		
	}
	
	@ApiOperation (value = "Consulta punto mas cercano", notes =  "Consulta de la Sucursal o Punto de retiro mas cercano a un punto dado.")
	@GetMapping("/proximo/{latitude}/{longitude}")
	public NodeDTO getNearestNode(@PathVariable Double latitude, @PathVariable Double longitude) {
		return modelMapper.mapHeritageNode(nodeService.getNearestNode(latitude, longitude));
	}
	
	
	@DeleteMapping("{id}")
	@ResponseStatus(code = HttpStatus.OK)
	@ApiOperation (value = "Borrar Nodo", notes =  "Se borra Sucursal o Punto de retiro para un ID dado.")
	public void deleteNode(@PathVariable Integer id) {
		nodeService.deleteNode(id);
	}
	

    @ApiOperation (value = "Registra un tipo una Sucursal o Punto de retiro", notes =  " Registra un tipo una Sucursal o Punto de retiro"
    		+ " \n Ejemplo de prueba \n {        \r\n"
    		+ "        \"latitude\": -31.4106736,\r\n"
    		+ "        \"longitude\": -64.1795139,\r\n"
    		+ "        \"nodeType\": \"STORE\",\r\n"
    		+ "        \"openTime\": \"08:00\",\r\n"
    		+ "        \"closeTime\": \"20:00\",\r\n"
    		+ "        \"address\": \"Domingo F Sarmiento esquina General Paz\"\r\n"
    		+ "    }\r\n"
    		+ "")    
	@PostMapping("")
	public NodeDTO saveNode(@RequestBody() NodeDTO nodeDTO) {
		Node node = modelMapper.mapHeritageNodeDTO(nodeDTO);
		return modelMapper.mapHeritageNode(nodeService.saveNode(node));
	}
	
    @ApiOperation (value = "Se actualiza información", notes =  "Se actualiza la informacion correspondiente a una Sucursal o Punto de retiro"
    		+ "		\n	Ejemplo de prueba \n {        \r\n"
    		+ "        \"id\": 1,\r\n"
    		+ "        \"latitude\": -31.4106736,\r\n"
    		+ "        \"longitude\": -64.1795139,\r\n"
    		+ "        \"nodeType\": \"STORE\",\r\n"
    		+ "        \"openTime\": \"08:00\",\r\n"
    		+ "        \"closeTime\": \"18:00\",\r\n"
    		+ "        \"address\": \"Domingo F Sarmiento 141\"\r\n"
    		+ "    }\r\n"
    		+ "")   
	@PutMapping("")
	public NodeDTO updateNode(@RequestBody() NodeDTO nodeDTO) {
		Node node = modelMapper.mapHeritageNodeDTO(nodeDTO);
		return modelMapper.mapHeritageNode(nodeService.updateNode(node));
	}
	
    @ApiOperation (value = "Lista de Sucursales-Punto de retiro.", notes =  "Lista todos las Sucursales y Puntos de retiro registrados hasta el momento.")
	@GetMapping("/listar")
	public List<NodeDTO> listarNodos(){
		List<Node> nodes = nodeService.listarNodos();
		return modelMapper.mapHeritageNode(nodes);
	}
}
