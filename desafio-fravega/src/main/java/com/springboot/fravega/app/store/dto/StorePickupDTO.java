package com.springboot.fravega.app.store.dto;

import com.fasterxml.jackson.annotation.JsonTypeName;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


//@ApiModel(parent = NodeDTO.class)
@NoArgsConstructor
@Getter 
@Setter
@JsonTypeName("STORE_PICKUP")
public class StorePickupDTO extends NodeDTO {
	private Long capacity;	
}
