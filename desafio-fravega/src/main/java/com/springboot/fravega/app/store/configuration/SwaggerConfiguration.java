package com.springboot.fravega.app.store.configuration;

import java.util.Collections;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.service.Contact;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfiguration {

	@Bean
	public Docket configuration() {
		return new 	Docket(DocumentationType.SWAGGER_2)
					.select()
					.paths(PathSelectors.ant("/api/**"))
					.apis(RequestHandlerSelectors.basePackage("com.springboot.fravega.app.store"))
					.build()
					.apiInfo(getApiInfo())
					;
	}
	
	private ApiInfo getApiInfo() {
		return new ApiInfo(
				"Fravega Nodes API",
				"API destinada a registrar Sucursales o Puntos de retiro y realizar consultes sobre los mismos.",
				"1.0",
				"http://codmind.com/terms",
				new Contact("Soporte IT Fravega", "https://fravega.com/it", "sorpoteit@fravega.com"),
				"LICENSE",
				"LICENSE URL",
				Collections.emptyList()
				);
	}
}
