package com.springboot.fravega.app.store;

import static org.junit.jupiter.api.Assertions.*;


import java.text.ParseException;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;

import com.springboot.fravega.app.store.entity.Node;
import com.springboot.fravega.app.store.entity.Store;
import com.springboot.fravega.app.store.entity.StorePickup;
import com.springboot.fravega.app.store.exceptions.EntityAlreadyExistException;
import com.springboot.fravega.app.store.exceptions.EntityNotExistException;
import com.springboot.fravega.app.store.model.service.INodeService;
import com.springboot.fravega.app.store.routes.GeographicCoordinate;


@SpringBootTest
@ActiveProfiles("testing")
@DirtiesContext(classMode = ClassMode.BEFORE_EACH_TEST_METHOD)
@Sql(scripts ={"/data-test-import.sql"})
class DesafioFravegaApplicationTests {
	
	@Autowired
	@Qualifier("nodeServiceImpl")
	private INodeService nodeService;
	
	
	@Test
	void contextLoads() {
	}

	@Test
	public void saveNodeMethod() throws ParseException {
		
		Node store = nodeService.saveNode(this.buildStoreObject());
		Node storePickup = nodeService.saveNode(this.buildStorePickupObject());
		
		assertNotNull(store.getId(),"Store doesn't save it.");
		assertTrue(store instanceof Store, "Doesn't match class instance");
		assertNotNull(storePickup.getId(), "StorePickup doesn't save it.");	
		assertTrue(storePickup instanceof StorePickup, "Doesn't match class instance");
		assertThrowsExactly(EntityAlreadyExistException.class, ()-> nodeService.saveNode(this.buildStoreObject()), "No debe ingresarse una sucursal con la misma coordenada");
	}
	
	@Test
	public void updateNodeMethod() throws ParseException {
		
		Store store = (Store)nodeService.getNode(1);
		String newAddres = "Direccion de prueba.";
		store.setAddress(newAddres);
		Store storeModified = (Store)nodeService.updateNode(store);		
		
		assertEquals(newAddres, storeModified.getAddress());
		assertEquals(store.getId(),storeModified.getId());
		
		Store storeModifiedException = (Store)this.buildStoreObject();
		storeModifiedException.setId(1000);
		assertThrowsExactly(EntityNotExistException.class,()-> nodeService.updateNode(storeModifiedException));
		
	}
	
	@Test()
	public void getNodeMethod() {
		final Integer nodeExpectedId = 1;
		Node handleNode = nodeService.getNode(nodeExpectedId);	
		assertTrue(handleNode instanceof Store,"Doesn't match class instance");
		
		Double latitudeExpected = -31.4106736;
		Double longitudeExpected = -64.1795139;
		assertEquals(latitudeExpected, handleNode.getLatitude(), "Latitude does't match");
		assertEquals(longitudeExpected, handleNode.getLongitude(), "Longitude does't match");
	}
	
	@Test()
	public void listarNodos() {
		List<Node> nodeList = nodeService.listarNodos();
		assertEquals(nodeList.size(), 3, "Quantity of Nodes expected doesn't match");
	} 
	
	@Test()
	public void getNearestNode() {
		
		GeographicCoordinate expectedNearestPoint = GeographicCoordinate.builder()
														.latitude(-31.4106736)
														.longitude(-64.1795139)
														.build();
		GeographicCoordinate testPint = GeographicCoordinate.builder()
														.latitude(-31.40940081025194)
														.longitude( -64.18415948876012)
														.build();
		Node nearestNode = nodeService.getNearestNode(testPint.getLatitude(), testPint.getLatitude());
		
		GeographicCoordinate actualdNearestPoint = GeographicCoordinate.builder()
				.latitude(nearestNode.getLatitude())
				.longitude(nearestNode.getLongitude())
				.build();
		
		assertEquals(expectedNearestPoint, actualdNearestPoint);		
	}
	
	@Test
	public void deleteNode() {
		final Integer nodeExpectedId = 1;
		assertNotNull(nodeService.getNode(nodeExpectedId), "Expected Node doesn't exist");
		nodeService.deleteNode(nodeExpectedId);
		assertThrowsExactly(EntityNotExistException.class, () -> nodeService.getNode(nodeExpectedId), "Node wasn't deleted as expected");
		
	}
	
	
	
	private Store buildStoreObject() {
		try {
		return  Store.builder()	
							.address("Calle Prueba 1")
							.closeTime(getCloseTimeDefault())
							.openTime(getOpenTimeDefault())
							.latitude(-31.449158510346212)
							.longitude(-64.18307637977452)							
							.build();
		}catch(Exception e) {
			return null;
		}
	}
	
	private StorePickup buildStorePickupObject() {
		try {
			return  StorePickup.builder()	
					.latitude(-31.463540655630936)
					.longitude(-64.24643629854151)
					.capacity(1000L)
					.build();
			
		}catch(Exception ex) {
			return null;
		}
		
		}
	
	private LocalTime getOpenTimeDefault()  throws ParseException{
		return LocalTime.parse("08:00", DateTimeFormatter.ofPattern("HH:mm"));	
	}
	
	private LocalTime getCloseTimeDefault()  throws ParseException{		
		return LocalTime.parse("18:00", DateTimeFormatter.ofPattern("HH:mm"));		
	}
	
	

}
